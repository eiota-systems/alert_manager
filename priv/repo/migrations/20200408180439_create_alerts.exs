defmodule AlertManager.Repo.Migrations.CreateAlerts do
  use Ecto.Migration

  def change do
    create table("alerts", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:realm, :string, null: false)
      add(:type, :string, null: false)
      add(:name, :string, null: false)
      add(:path, :string, null: false)
      add(:window, :integer, null: false)
      add(:trigger, :integer, null: false)
      add(:level, :string, null: false)
      add(:active, :boolean, default: true)
      add(:aggregation, :string)
      add(:function, :string)
      add(:event, :string)
      timestamps()
    end

    create(unique_index(:alerts, [:realm, :type, :name]))

    create table("notifications", primary_key: false) do
      add(:id, :binary_id, primary_key: true, default: fragment("gen_random_uuid()"))
      add(:alert_id, references(:alerts, type: :binary_id, on_delete: :delete_all), null: false)
      add(:status, :string, null: false)
      timestamps()
    end

    create(index(:notifications, [:alert_id, :status]))
  end
end
