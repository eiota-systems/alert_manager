# AlertManager

This application manages alerts and notifications.

## Alerts

These are rules based on a configurable sliding window.

We can alert on metrics, aka float values, or events, aka strings.

A metric alert has the form

```elixir
%{
  realm: "org.entropealabs.clubhouse",
  type: "metric",
  name: "VOC Levels",
  window: 15_000,
  aggregation: "avg",
  level: "guarded",
  path: "$.rosetta.*.air_quality.voc",
  function: "gt",
  trigger: 25
}
```

This translates into

`Alert at the guarded level if the VOC level of any Rosetta device in the realm org.entropealabs.clubhouse exceeds an average of 25 within a 15 second window`


An event alert has the form

```elixir
{
  realm: "org.entropealabs.clubhouse",
  type: "event",
  name: "Chiller Status",
  window: 7_000,
  level: "guarded",
  path: "$.rosetta.*.hvac.chiller.status",
  event: "off",
  function: "gt",
  trigger: 10
}
```

This translates into

`Alert at the guarded level if the Chiller status of any Rosetta device in the realm org.entropealabs.clubhouse turns off more than 10 times in 7 seconds`

### realm

Which realm the alert applies to, we will subscribe to all events published by the device shadow in this realm, the topics are `device.metrics` and `device.events`. We only process events that apply to a known alert at the time of the receiving the published payload.

### type

This is either `metric` or `event`

### name

a human readable name for display purposes

### window

the duration of our sliding window queue in milliseconds

### aggregation

The aggregate function to apply to all values in our sliding window queue.

Supported functions for alerts of type `metric`
* `avg`
* `sum`

Supported functions for alerts of type `event`
* `count`

### level

The level to be applied to the alert and notification if triggered.

Available values

* `low`
* `guarded`
* `elevated`
* `high`
* `severe`

### path

A JSONPath that applies to the published event payload to extract the value for our alert

### function

The function used to evaluate our aggregate against our trigger value

Supported values
* `gt`
* `gte`
* `lt`
* `lte`
* `eq`

### trigger

The value used for comparison against our aggregate function, applied using the `function`

eg; `avg(sliding_window) >= trigger`

## Notifications

Notifications are created if any `alert` rule is triggered, and hasn't had a notification created and published yet.

A notification takes the form of

```elixir
%{
  id: uuid,
  status: 'pending',
  created_at: datetime
  alert: %{
    ...alert
  }
}
```

This will be published into the realm configured in the `alert` under the topic `device.alert`

Available statuses include
* `pending`
* `aknowledged`
* `escalate`
* `resolved`
