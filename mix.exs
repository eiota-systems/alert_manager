defmodule AlertManager.MixProject do
  use Mix.Project

  def project do
    [
      app: :alert_manager,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      releases: releases(),
      deps: deps()
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      mod: {AlertManager, []}
    ]
  end

  defp releases do
    [
      app: [
        include_executables_for: [:unix],
        applications: [alert_manager: :permanent, runtime_tools: :permanent],
        cookie: "alert_manager_cookie"
      ]
    ]
  end

  defp deps do
    [
      {:ecto_sql, "~> 3.0"},
      {:elixpath, "~> 0.1.0"},
      {:libcluster, "~> 3.2"},
      {:libring, "~> 1.5"},
      {:postgrex, ">= 0.0.0"},
      {:wampex_client,
       git: "https://gitlab.com/entropealabs/wampex_client.git",
       tag: "93704158ba2fb4db7bb19a454dad3367ecf5d03f"}
    ]
  end
end
