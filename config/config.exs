use Mix.Config

config :logger,
  level: :info,
  compile_time_purge_matching: [
    [level_lower_than: :info]
  ]

config :alert_manager, ecto_repos: [AlertManager.Repo]

config :alert_manager, AlertManager.Repo,
  database: "alert_manager",
  hostname: "localhost",
  port: 26_257,
  username: "root"

config :alert_manager,
  topologies: [
    kv: [
      strategy: Cluster.Strategy.Epmd,
      config: [
        hosts: [
          :"app@alert-manager1.eiota.systems",
          :"app@alert-manager2.eiota.systems",
          :"app@alert-manager3.eiota.systems"
        ]
      ],
      connect: {:net_kernel, :connect_node, []},
      disconnect: {:erlang, :disconnect_node, []},
      list_nodes: {:erlang, :nodes, [:connected]}
    ]
  ]
