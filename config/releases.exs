import Config

config :alert_manager, AlertManager.Repo, hostname: System.get_env("DATABASE_HOSTNAME")
