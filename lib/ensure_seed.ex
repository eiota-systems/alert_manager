defmodule AlertManager.EnsureSeed do
  @moduledoc false
  require Logger
  use GenServer

  alias AlertManager.Repo
  @ecto_repos [Repo]

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok)
  end

  def init(:ok) do
    ensure_migrations()
    :ignore
  end

  defp ensure_migrations do
    Logger.info("Ensuring tables have been migrated")

    for repo <- @ecto_repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end
end
