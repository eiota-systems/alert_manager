defmodule AlertManager.AlertSupervisor do
  use DynamicSupervisor

  alias AlertManager.{Alert, AlertWindow}

  def start_link(_) do
    DynamicSupervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @impl true
  def init(:ok) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  def start_alert(%Alert{} = alert) do
    spec = %{id: alert.id, start: {AlertWindow, :start_link, [alert]}}
    DynamicSupervisor.start_child(__MODULE__, spec)
  end
end
