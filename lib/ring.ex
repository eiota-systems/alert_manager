defmodule AlertManager.Ring do
  use GenServer

  def get_node(key) do
    GenServer.call(__MODULE__, {:get_node, key})
  end

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    ring = HashRing.new(Node.self())
    :net_kernel.monitor_nodes(true, node_type: :all)
    {:ok, %{ring: ring}}
  end

  def handle_info({:nodeup, node, _}, %{ring: ring} = state) do
    {:noreply, %{state | ring: HashRing.add_node(ring, node)}}
  end

  def handle_info({:nodedown, node, _}, %{ring: ring} = state) do
    {:noreply, %{state | ring: HashRing.remove_node(ring, node)}}
  end

  def handle_call({:get_node, key}, _, %{ring: ring} = state) do
    {:reply, HashRing.key_to_node(ring, key), state}
  end
end
