defmodule AlertManager.Alert do
  use Ecto.Schema
  require Logger

  alias __MODULE__
  alias AlertManager.{Notification, Repo}

  require Ecto.Query

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "alerts" do
    field(:realm, :string)
    field(:type, :string)
    field(:name, :string)
    field(:path, :string)
    field(:level, :string)
    field(:window, :integer)
    field(:trigger, :integer)
    field(:active, :boolean)
    field(:aggregation, :string)
    field(:function, :string)
    field(:event, :string)
    has_many(:notifications, Notification)
    timestamps()
  end

  def to_map(alert) do
    %{
      id: alert.id,
      realm: alert.realm,
      type: alert.type,
      name: alert.name,
      path: alert.path,
      level: alert.level,
      window: alert.window,
      trigger: alert.trigger,
      aggregation: alert.aggregation,
      function: alert.function,
      event: alert.event
    }
  end

  def get(realm: realm, name: name, type: type) do
    Alert
    |> Repo.get_by!(realm: realm, name: name, type: type)
  end

  def create(%Alert{} = alert) do
    {:ok, r} =
      alert
      |> Repo.preload([:notifications])
      |> Repo.insert()

    r
  rescue
    er ->
      Logger.error("#{inspect(er)}")
      get(realm: alert.realm, name: alert.name, type: alert.type)
  end

  def all do
    Repo.all(Alert)
  end

  def get_by_realm(realm: realm) do
    Alert
    |> Ecto.Query.where(realm: ^realm)
    |> Repo.all()
  end
end
