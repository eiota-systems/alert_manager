defmodule AlertManager.EventProducer do
  use GenServer

  require Logger

  alias AlertManager.Alert

  @ids [
    "k1j2h3",
    "2lk3j4",
    "0a9s8d",
    "9a8s7d98a7sd",
    "xpocixp4"
  ]

  @topics [
    {"air_quality.pm", 0..10},
    {"air_quality.voc", 0..30},
    {"air_quality.co2", 399..2000},
    {"hvac.chiller.status", ["on", "off"]},
    {"hvac.boiler.temp", 16..42}
  ]

  def start_link(realm: realm) do
    GenServer.start_link(__MODULE__, realm)
  end

  def init(realm) do
    # connect to realm, subscribe to events from device_shadow
    Process.send_after(self(), :fill_queue, 0)
    {:ok, %{alerts: []}, {:continue, realm}}
  end

  def handle_continue(realm, state) do
    alerts = Alert.get_by_realm(realm: realm)
    {:noreply, %{state | alerts: alerts}}
  end

  def handle_info(:fill_queue, %{alerts: alerts} = state) do
    Enum.map(0..(Enum.count(@ids) - 1), fn id ->
      @ids
      |> Enum.at(id)
      |> get_message()
      |> expand_metrics()
      |> zip_alert(alerts)
      |> cast_to_alert()
    end)

    Process.send_after(self(), :fill_queue, 800)
    {:noreply, state}
  end

  def cast_to_alert(events) do
    Enum.each(events, fn
      :not_alert -> :noop
      {id, {_pid, _val} = event} -> GenServer.cast({:via, :swarm, id}, {:event, event})
    end)
  end

  def get_message(id) do
    %{
      publish_id: Enum.random(10000..100_000_000),
      arg_list:
        Enum.map(@topics, fn {topic, range} ->
          ["rosetta.#{id}.#{topic}", Enum.random(range)]
        end),
      arg_kw: %{
        "id" => id,
        "categories" => [],
        "type" => "rosetta",
        "tenant" => "org.entropealabs"
      }
    }
  end

  def expand_metrics(%{publish_id: pid, arg_list: metrics}) do
    Enum.map(metrics, fn [k, v] ->
      %{id: pid, data: create_map(k, v)}
    end)
  end

  def create_map(k, v) do
    [h | _] = keys = String.split(k, ".")
    put_in(%{h => %{}}, Enum.map(keys, &Access.key(&1, %{})), v)
  end

  def zip_alert(metrics, alerts) do
    Enum.flat_map(metrics, fn %{id: id, data: data} ->
      Enum.map(alerts, fn %Alert{id: aid, path: path} ->
        case get_alert_value(data, path) do
          :not_alert -> :not_alert
          val -> {aid, {id, val}}
        end
      end)
    end)
  end

  def get_alert_value(data, path) do
    case Elixpath.query(data, path) do
      {:ok, []} -> :not_alert
      {:ok, [val]} -> val
    end
  end
end
