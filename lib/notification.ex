defmodule AlertManager.Notification do
  use Ecto.Schema
  import Ecto.Query

  alias __MODULE__
  alias AlertManager.{Alert, Repo}

  @primary_key {:id, :binary_id, autogenerate: false, read_after_writes: true}
  @foreign_key_type :binary_id
  schema "notifications" do
    belongs_to(:alert, Alert)
    field(:status, :string)
    timestamps()
  end

  def get_latest(alert: %Alert{id: id}) do
    from(n in Notification, where: n.alert_id == ^id)
    |> first(:inserted_at)
    |> Repo.one()
  end

  def create(%Notification{} = notif) do
    {:ok, r} =
      notif
      |> Repo.insert()

    r
  end

  def to_map(alert, notification) do
    %{
      id: notification.id,
      status: notification.status,
      created_at: NaiveDateTime.to_iso8601(notification.inserted_at),
      alert: Alert.to_map(alert)
    }
  end
end
