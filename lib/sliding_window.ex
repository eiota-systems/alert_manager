defmodule AlertManager.SlidingWindow do
  alias __MODULE__

  @type milliseconds :: non_neg_integer()
  @type event :: {id :: non_neg_integer(), event :: binary() | number()}
  @type item :: {timestamp :: integer(), event()}
  @type queue :: {[item()], [item()]}

  @enforce_keys [:milliseconds, :queue]
  defstruct [:milliseconds, :queue]

  @type t :: %__MODULE__{
          milliseconds: milliseconds(),
          queue: queue()
        }

  @spec new(milliseconds :: non_neg_integer()) :: t()
  def new(milliseconds) do
    %SlidingWindow{milliseconds: milliseconds, queue: :queue.new()}
  end

  @spec add(t(), item()) :: t()
  def add(%SlidingWindow{milliseconds: ms, queue: q} = sw, item) do
    ts = timestamp()
    q = remove_old_items(ts, ms, q)
    q = :queue.in({ts, item}, q)
    %SlidingWindow{sw | queue: q}
  end

  @spec get(t()) :: [term()]
  def get(%SlidingWindow{queue: q}) do
    q
    |> :queue.to_list()
    |> Enum.map(&elem(&1, 1))
  end

  @spec remove_old_items(now :: integer(), duration :: milliseconds(), queue :: queue()) ::
          queue()
  def remove_old_items(now, duration, queue) do
    case :queue.is_empty(queue) do
      false -> do_remove(now, duration, queue)
      true -> queue
    end
  end

  def do_remove(now, duration, queue) do
    case :queue.get(queue) do
      :empty -> queue
      {ts, _} when now - ts > duration -> remove_old_items(now, duration, :queue.drop(queue))
      _ -> queue
    end
  end

  @spec timestamp() :: integer()
  def timestamp, do: :erlang.monotonic_time(:millisecond)
end
