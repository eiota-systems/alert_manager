defmodule AlertManager.RealmWorker do
  use GenServer
  require Logger

  alias AlertManager.{Alert, Repo}
  alias Wampex.Client
  alias Wampex.Roles.Broker.Event
  alias Wampex.Roles.Callee.{Register, Yield}
  alias Wampex.Roles.Dealer.Invocation
  alias Wampex.Roles.Subscriber.Subscribe

  @add "alerts.add"
  @update "alerts.update"
  @update_notification "alerts.update_notification"

  def start_link(tenant: tenant, realm: realm, client: client, tasks: tasks, agent: agent) do
    GenServer.start_link(__MODULE__, {tenant, realm, client, tasks, agent},
      name: worker_name(realm)
    )
  end

  @impl true
  def init({tenant, realm, client, tasks, agent}) do
    Client.add(client, self())
    register(client)
    alerts = Alert.get_by_realm(realm: realm)
    Logger.info("Realm Alerts: #{inspect(alerts)}")
    subscribe(client)

    {:ok,
     %{
       tenant: tenant,
       realm: realm,
       client: client,
       agent: agent,
       tasks: tasks,
       alerts: alerts
     }}
  end

  @impl true
  def handle_info({:connected, _client}, state) do
    {:noreply, state}
  end

  @impl true
  def handle_info(
        %Invocation{request_id: rid, arg_kw: alert, details: %{"procedure" => "alerts.add"}},
        %{client: client} = state
      ) do
    %Alert{id: id} =
      Alert
      |> Repo.load(alert)
      |> Alert.create()

    Client.yield(client, %Yield{request_id: rid, arg_kw: %{success: id}})
    {:noreply, state}
  end

  @impl true
  def handle_info(%Event{} = event, %{tasks: tasks, alerts: alerts} = state) do
    Task.Supervisor.start_child(tasks, fn ->
      event
      |> expand_metrics()
      |> zip_alert(alerts)
      |> cast_to_alert()
    end)

    {:noreply, state}
  end

  @impl true
  def handle_info(event, state) do
    Logger.warn("Realm #{state.realm} received event #{inspect(event)}")
    {:noreply, state}
  end

  def cast_to_alert(events) do
    Enum.each(events, fn
      :not_alert ->
        :noop

      {id, {_pid, _val} = event} ->
        GenServer.cast({:global, id}, {:event, event})
    end)
  end

  def expand_metrics(%Event{publication_id: pid, arg_list: metrics}) do
    Enum.map(metrics, fn [k, v] ->
      %{id: pid, data: create_map(k, v)}
    end)
  end

  def create_map(k, v) do
    [h | _] = keys = String.split(k, ".")
    put_in(%{h => %{}}, Enum.map(keys, &Access.key(&1, %{})), v)
  end

  def zip_alert(metrics, alerts) do
    Enum.flat_map(metrics, fn %{id: id, data: data} ->
      Enum.map(alerts, fn %Alert{id: aid, path: path} ->
        case get_alert_value(data, path) do
          :not_alert -> :not_alert
          val -> {aid, {id, val}}
        end
      end)
    end)
  end

  def get_alert_value(data, path) do
    case Elixpath.query(data, path) do
      {:ok, []} -> :not_alert
      {:ok, [val]} -> val
    end
  end

  defp subscribe(client) do
    r = Client.subscribe(client, %Subscribe{topic: "device.metrics"})
    r1 = Client.subscribe(client, %Subscribe{topic: "device.events"})
    Logger.info("Metric Subscription: #{inspect(r)}")
    Logger.info("Event Subscription: #{inspect(r1)}")
  end

  defp register(client) do
    do_register(client, @add)
    do_register(client, @update)
    do_register(client, @update_notification)
  end

  defp do_register(client, proc) do
    case Client.register(client, %Register{procedure: proc}) do
      {:ok, id} ->
        id

      _r ->
        :timer.sleep(100)
        do_register(client, proc)
    end
  end

  defp worker_name(realm), do: Module.concat([__MODULE__, realm, Worker])
end
