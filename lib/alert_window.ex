defmodule AlertManager.AlertWindow do
  use GenServer

  require Logger
  alias AlertManager.{Alert, Notification, RealmClient, SlidingWindow}

  # @alert_levels [
  #  :low,
  #  :guarded,
  #  :elevated,
  #  :high,
  #  :severe
  # ]

  # @notification_statuses [
  #  :pending,
  #  :acknowledged,
  #  :escalate,
  #  :resolved
  # ]

  def start_link(alert) do
    GenServer.start_link(__MODULE__, alert, name: {:global, alert.id})
  end

  @impl true
  def init(%Alert{window: window} = alert) do
    {:ok, %{alert: alert, sliding_window: SlidingWindow.new(window)}}
  end

  @impl true
  def handle_cast({:event, {_id, _value} = event}, %{alert: alert, sliding_window: sw} = state) do
    {:noreply, %{state | sliding_window: handle_event(event, sw, alert)}}
  end

  @spec handle_event(
          event :: SlidingWindow.event(),
          sliding_window :: SlidingWindow.t(),
          alert :: %Alert{}
        ) ::
          SlidingWindow.t()
  defp handle_event(event, sliding_window, alert) do
    case maybe_add_event(event, sliding_window) do
      %SlidingWindow{} = sw ->
        maybe_alert(alert, SlidingWindow.get(sw))
        sw

      _ ->
        sliding_window
    end
  end

  defp maybe_add_event(event, sw) do
    case event_exists?(event, sw) do
      true -> :noop
      false -> SlidingWindow.add(sw, event)
    end
  end

  defp event_exists?(event, sw) do
    sw
    |> SlidingWindow.get()
    |> Enum.any?(fn e -> e == event end)
  end

  defp maybe_alert(%Alert{} = alert, items) do
    # case alert.type do
    #  "metric" ->
    #    Logger.info("Checking #{alert.name} #{alert.trigger} #{alert.function} #{avg(items)}")
    #
    # "event" ->
    #    Logger.info(
    #     "Checking #{alert.name} #{alert.trigger} #{alert.function} #{count(items, alert.event)}"
    #   )
    # end

    case trigger?(alert, items) do
      true -> alert(alert)
      false -> :noop
    end
  end

  defp alert(%Alert{} = alert) do
    case maybe_create_notification(alert) do
      :noop -> :noop
      %Notification{} = notif -> publish_notification(alert, notif)
    end
  end

  defp maybe_create_notification(alert) do
    case Notification.get_latest(alert: alert) do
      nil -> create_notification(alert)
      %Notification{status: "resolved"} -> create_notification(alert)
      _ -> :noop
    end
  end

  def create_notification(alert) do
    Logger.info("Creating new notification for #{alert.name}")
    Notification.create(%Notification{alert: alert, status: "pending"})
  end

  defp publish_notification(alert, notification) do
    notif = Notification.to_map(alert, notification)
    RealmClient.publish_notification(alert.realm, notif)
  end

  defp trigger?(
         %Alert{type: "metric", aggregation: "avg", trigger: trigger, function: func},
         items
       ) do
    compare(func, avg(items), trigger)
  end

  defp trigger?(
         %Alert{type: "metric", aggregation: "sum", trigger: trigger, function: func},
         items
       ) do
    compare(func, sum(items), trigger)
  end

  defp trigger?(%Alert{type: "event", event: event, trigger: trigger, function: func}, items)
       when is_binary(event) do
    compare(func, count(items, event), trigger)
  end

  defp trigger?(_, _), do: false

  defp compare("gt", val, trigger), do: val > trigger
  defp compare("gte", val, trigger), do: val >= trigger
  defp compare("lt", val, trigger), do: val < trigger
  defp compare("lte", val, trigger), do: val <= trigger
  defp compare("eq", val, trigger), do: val == trigger
  defp compare(_, _, _), do: false

  def count(items, event) do
    Enum.count(items, fn
      {_, ^event} -> true
      _ -> false
    end)
  end

  def avg(items) do
    sum(items) / length(items)
  end

  defp sum(items) do
    Enum.reduce(items, 0, fn {_, val}, acc -> acc + val end)
  end
end
