defmodule AlertManager.AlertSync do
  use GenServer

  require Logger

  alias AlertManager.{Alert, AlertSupervisor, Ring}

  def start_link(_) do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  @impl true
  def init(:ok) do
    Process.send_after(self(), :load_alerts, Enum.random(20_000..30_000))
    {:ok, %{last_load: nil}}
  end

  @impl true
  def handle_info(:load_alerts, state) do
    alerts = Alert.all()
    me = Node.self()

    Enum.each(alerts, fn alert ->
      case Ring.get_node(alert.id) do
        ^me -> AlertSupervisor.start_alert(alert)
        _ -> :noop
      end
    end)

    Process.send_after(self(), :load_alerts, Enum.random(20_000..30_000))
    {:noreply, state}
  end
end
