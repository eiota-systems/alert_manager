defmodule AlertManager.RealmClient do
  use Supervisor

  require Logger

  alias AlertManager.{RealmWorker, RealmWorkerAgent}
  alias Wampex.Client
  alias Wampex.Client.Session
  alias Wampex.Client.{Authentication, Realm}
  alias Wampex.Roles.{Callee, Caller, Subscriber}
  alias Wampex.Roles.Publisher.Publish

  def start_link(password, url, realm, tenant) do
    Supervisor.start_link(__MODULE__, {password, url, realm, tenant}, name: sup_name(realm))
  end

  @impl true
  def init({password, url, realm, tenant}) do
    authentication = %Authentication{
      authid: "platform@" <> realm,
      authmethods: ["wampcra"],
      secret: password
    }

    r = %Realm{name: realm, authentication: authentication}

    roles = [Callee, Caller, Subscriber]
    session = %Session{url: url, realm: r, roles: roles}

    realm_client = client_name(realm)
    realm_agent = agent_name(realm)
    realm_tasks = task_name(realm)

    children = [
      {Client, name: realm_client, session: session, reconnect: false},
      {RealmWorkerAgent, name: realm_agent},
      {Task.Supervisor, name: realm_tasks},
      {RealmWorker,
       tenant: tenant, realm: realm, client: realm_client, tasks: realm_tasks, agent: realm_agent}
    ]

    opts = [strategy: :rest_for_one]
    Supervisor.init(children, opts)
  end

  def publish_notification(realm, notif) do
    Client.publish(client_name(realm), %Publish{topic: "device.alert", arg_kw: notif})
  end

  defp sup_name(realm), do: Module.concat([__MODULE__, realm, Sup])
  defp client_name(realm), do: Module.concat([__MODULE__, realm, Cli])
  defp agent_name(realm), do: Module.concat([__MODULE__, realm, Agent])
  defp task_name(realm), do: Module.concat([__MODULE__, realm, Tasks])
end
