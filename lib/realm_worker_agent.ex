defmodule AlertManager.RealmWorkerAgent do
  use Agent

  def start_link(name: name) do
    Agent.start_link(fn -> [] end, name: name)
  end

  def add_profile(name, {_id, _profile} = prof) do
    Agent.update(name, &Enum.uniq([prof | &1]))
  end

  def get_profiles(name) do
    Agent.get(name, & &1)
  end
end
